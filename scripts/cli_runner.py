#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import re
import requests
import subprocess
from shlex import quote


def get_config():
    return {
        "debug": False,
        "exts_skins_file": "https://noc.wikimedia.org/conf/wmf-config/extension-list",
        "wchc_cmd": "poetry run wchc -c ",
    }


def get_extensions_skins_list(config):
    exts_list = []
    skins_list = []

    resp = requests.get(config["exts_skins_file"], timeout=16)
    if resp.status_code == 200:
        file_data = resp.text.split("\n")
        for line in file_data:
            ext_search = re.search(
                r"^\$IP\/extensions\/([\w]+)\/extension\.json$", line.strip()
            )
            if ext_search is not None and ext_search.group(1) is not None:
                exts_list.append(ext_search.group(1))
            skin_search = re.search(r"^\$IP\/skins\/([\w]+)\/skin\.json$", line.strip())
            if skin_search is not None and skin_search.group(1) is not None:
                skins_list.append(skin_search.group(1))
    else:
        raise ValueError("Invalid extensions and skins configuration url provided!")

    return exts_list + skins_list


def run_wchc_scans(config, target_projects=[]):
    count = 0
    for proj in target_projects:
        if config["debug"] and count > 3:
            break
        try:
            cmd = config["wchc_cmd"] + quote(proj)
            wchc_tool_out = subprocess.run(
                cmd.split(),
                stdout=subprocess.PIPE,
                stderr=subprocess.DEVNULL,
                check=True,
                universal_newlines=True,
            )
        except subprocess.CalledProcessError as e:
            print(e.output)
        finally:
            wchc_results = wchc_tool_out.stdout
        print(wchc_results, end="")
        count += 1


def cli_runner():
    cfg = get_config()
    esl = get_extensions_skins_list(cfg)
    run_wchc_scans(cfg, esl)


if __name__ == "__main__":
    cli_runner()
