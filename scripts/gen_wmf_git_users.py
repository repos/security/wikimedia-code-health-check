#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# License: Apache 2.0


import argparse
import csv
import requests
import time
from lxml import html
from pathvalidate import sanitize_filepath


def get_config():
    return {
        "ldap_base_url": "https://ldap.toolforge.org",
        "wmf_ldap_url": "https://ldap.toolforge.org/group/wmf",
        "csv_file_path": "includes/wmf_staff.csv",
        "edge_cases_add": {
            "Timo Tijhof": "krinkle@fastmail.com",
            "C. Scott Ananian": "cscott@cscott.net",
            "seddon": "JOSEPHSEDDON@GMAIL.COM",
            "Bartosz Dziewoński": "matma.rex@gmail.com",
        },
        "edge_cases_remove": {
            "Brian Wolff": "bawolff+wn@gmail.com",
        },
    }


def parse_cli_args(config):
    args = {}
    unknown = {}

    parser = argparse.ArgumentParser()
    parser.add_argument(
        "csv_file_path",
        help="csv file path",
        type=str,
        nargs="?",
        default=config["csv_file_path"],
    )
    parser.add_argument("-d", "--debug", action="store_true", help="run in debug mode")
    parser.add_argument(
        "-l",
        "--limit",
        dest="limit",
        help="limit number of requests to ldap website",
        type=int,
    )

    args, unknown = parser.parse_known_args()
    return args


def update_config_from_cli_args(config, args):
    if len(args.csv_file_path) > 0:
        config["csv_file_path"] = args.csv_file_path

    config["debug"] = False
    if args.debug is True:
        config["debug"] = True

    config["limit"] = 0
    if args.limit is not None and args.limit >= 0:
        config["limit"] = args.limit

    return config


def search_ldap_api(config):
    http_response = requests.get(config["wmf_ldap_url"], timeout=5)
    if http_response.status_code == 200:
        doc = html.fromstring(http_response.text)

    user_urls = doc.xpath("//body//ul//li//a[2]/@href")
    user_urls = [config["ldap_base_url"] + ueu for ueu in user_urls]

    users = {}
    for i, ueu in enumerate(user_urls):
        if "limit" in config and i > config["limit"]:
            break  # early return after 10 queries in debug mode
        else:
            http_response = requests.get(ueu, timeout=60)
            if http_response.status_code == 200:
                doc = html.fromstring(http_response.text)

                user_name = doc.xpath("//body//ul[1]//li[1]//a[1]//text()")
                user_email = doc.xpath("//body//ul[1]//li[3]//code[1]//text()")

                users["".join(user_name)[5:]] = (
                    "".join(user_email).replace(" at ", "@").replace(" dot ", ".")
                )

                time.sleep(1)  # toolforge.org wasn't happy at times

    # add and remove edge cases
    users.update(config["edge_cases_add"])
    for un in users.copy():
        if (
            un in config["edge_cases_remove"].keys()
            or users[un] in config["edge_cases_remove"].values()
        ):
            del users[un]

    return users


def output_to_file_or_terminal(data, config):
    # debug - output to terminal
    if config["debug"] is True:
        print(data)
    else:
        with open(sanitize_filepath(config["csv_file_path"]), "w") as csvfile:
            writer = csv.writer(
                csvfile, delimiter=",", quotechar='"', quoting=csv.QUOTE_NONE
            )
            for un, ue in data.items():
                writer.writerow([un, ue])


def cli():
    c = get_config()
    a = parse_cli_args(c)
    c = update_config_from_cli_args(c, a)
    u = search_ldap_api(c)
    output_to_file_or_terminal(u, c)


if __name__ == "__main__":
    cli()
