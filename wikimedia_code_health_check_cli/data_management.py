#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import texttable


def calculate_weighted_risk_score(results):
    risk_weights = {
        "package_management": 0.8,
        "vulnerable_packages": 0.8,
        "sast": 0.5,
        "test_coverage": 0.6,
        "language_guidelines": 0.3,
        "non_auto_repo_commits": 0.6,
        "unique_contributors": 0.4,
        "contributor_concentration": 1,
        "staff_support": 1,
        "task_backlog": 0.7,
        "code_stewardship": 1,
    }
    total_risk = 0.0
    for c in risk_weights:
        if c in results:
            total_risk += float(risk_weights[c]) * float(results[c])
    return "%.2f" % total_risk


def output_report_results(
    search, project_git_branch, project_repo_url, results, csv_mode=False
):
    """calculate weighted risk total here"""
    results["weighted_risk"] = calculate_weighted_risk_score(results)

    """col name data can be used across multiple modes"""
    col_name_data = {
        "package_management": ["Pkg Mgmt", 8],
        "vulnerable_packages": ["Vuln Pkgs", 9],
        "sast": ["SAST", 4],
        "test_coverage": ["Test Cov", 8],
        "language_guidelines": ["Lang Guides", 11],
        "non_auto_repo_commits": ["Non-auto Cmts", 13],
        "unique_contributors": ["Uniq Contribs", 13],
        "contributor_concentration": ["Contrib Conc", 12],
        "staff_support": ["Staff Supp", 10],
        "task_backlog": ["Task Backlog", 12],
        "code_stewardship": ["Code Stew", 9],
        "weighted_risk": ["Weighted Risk", 13],
    }

    if csv_mode is False:
        # print pretty header for all modes
        print(f"PROJECT: {search}@{project_git_branch}")
        print(f"REPOSITORY: {project_repo_url}\n")

        col_defaults = {
            "col_align": "r",
            "col_valign": "t",
            "col_dtype": "t",
        }

        table = texttable.Texttable()
        col_count = 0
        for cnd in col_name_data:
            if cnd in results:
                col_count += 1

        table.set_cols_align([col_defaults["col_align"]] * col_count)
        table.set_cols_valign([col_defaults["col_valign"]] * col_count)
        table.set_cols_dtype([col_defaults["col_dtype"]] * col_count)

        col_widths = []
        col_hdr_names = []
        col_data = []
        for cnd in results:
            col_widths.append(col_name_data[cnd][1])
            col_hdr_names.append(col_name_data[cnd][0])
            col_data.append(results[cnd])
        table.set_cols_width(col_widths)
        table.add_rows([col_hdr_names, col_data])

        print(table.draw())

    else:
        """just print data - since csv will almost always be run
        in a batched mode, we should not print header fields"""
        print(f"{search}, {project_git_branch}, ", end="")
        for i, cnd in enumerate(results):
            if cnd in results:
                print(results[cnd], end="")
            else:
                print("", end="")
            if i + 1 < len(results):
                print(", ", end="")
            else:
                print("")
