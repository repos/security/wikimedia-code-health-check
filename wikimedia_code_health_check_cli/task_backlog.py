#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import requests
import json
import os
import re
from datetime import datetime
from dateutil.relativedelta import relativedelta


def get_task_backlog_config():
    phab_token = os.getenv("PHABRICATOR_TOKEN")
    if phab_token == "":
        raise ValueError(
            "Phabricator token environment variable not set - see README.md"
        )
    return {
        "phab_api_token": phab_token,  # global state from cli.py
        "phab_api_url": "https://phabricator.wikimedia.org/api/maniphest.search",
        "phab_projects_file": "includes/phab_projects.txt",
        "scores": {300: 10, 100: 5, 50: 2},
    }


def count_all(params):
    c = get_task_backlog_config()
    return_val = 0
    total = 0
    after = None

    res = requests.post(c["phab_api_url"], params, timeout=5)
    if res.status_code == 200:
        response = json.loads(res.text)
        if response is not None:
            if "result" in response and "data" in response["result"]:
                total = len(response["result"]["data"])
        if isinstance(response, dict):
            if (
                "cursor" in response["result"]
                and "after" in response["result"]["cursor"]
            ):
                after = response["result"]["cursor"]["after"]
    if after is None:
        return_val = total
    else:
        params["after"] = after
        return_val = total + count_all(params)
    return return_val


def backlog_size(project, filter=False):
    c = get_task_backlog_config()
    return_val = 0

    params = {
        "api.token": c["phab_api_token"],
        "constraints[projects][0]": project,
        "constraints[statuses][0]": "open",
        "constraints[statuses][1]": "stalled",
    }
    if filter:
        params["constraints[priority][0]"] = "needs triage"
    return_val = count_all(params)
    return return_val


def higher_priorities(project):
    c = get_task_backlog_config()
    return_val = 0

    params = {
        "api.token": c["phab_api_token"],
        "constraints[projects][0]": project,
        "constraints[statuses][0]": "open",
        "constraints[statuses][1]": "stalled",
        "constraints[statuses][2]": "in progress",
        "constraints[priorities][0]": "90",
        "constraints[priorities][1]": "80",
    }
    return_val = count_all(params)
    return return_val


def older_than_six_months(project):
    c = get_task_backlog_config()
    return_val = 0
    today = datetime.today()
    sixMonthsAgo = today - relativedelta(months=6)
    sixMonthsEpoch = int(sixMonthsAgo.timestamp())

    params = {
        "api.token": c["phab_api_token"],
        "constraints[projects][0]": project,
        "constraints[statuses][0]": "open",
        "constraints[statuses][1]": "stalled",
        "constraints[statuses][2]": "in progress",
        "constraints[createdEnd]": sixMonthsEpoch,
    }
    return_val = count_all(params)
    return return_val


def get_task_backlog_score(project_name):
    c = get_task_backlog_config()
    total_score = 0

    phab_prefixes = [
        "mediawiki-extensions-",
        "mediawiki-skins-",
        "mediawiki-services-",
        "",
    ]

    with open(c["phab_projects_file"]) as file:
        while line := file.readline():
            for p in phab_prefixes:
                re_pat = "^" + p + project_name
                if re.match(re_pat, line, re.I):
                    project_name = p + project_name
                    break

    size = backlog_size(project_name)
    highPriorities = higher_priorities(project_name)
    oldTasks = older_than_six_months(project_name)

    for s, v in c["scores"].items():
        if size > 0 and size > s:
            total_score = v
            break

    if total_score < 10 and highPriorities > 10:
        total_score += 1

    if total_score < 10 and oldTasks > 25:
        total_score += 1

    return total_score
