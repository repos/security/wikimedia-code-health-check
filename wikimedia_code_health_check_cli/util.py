#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import math
import subprocess
from shlex import quote as shlex_quote


def check_if_dependency_exists(cmd):
    """simple check of fs for bin/script"""
    return_val = False
    valid_cmd = subprocess.run(
        ["type", "-a", shlex_quote(cmd)],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.STDOUT,
    )
    if valid_cmd.returncode == 0:
        return_val = True
    return return_val


def standardize_score(config, old_score, new_scale_min=0, new_scale_max=10):
    """
    Score conversion from one scale to another using Linear transformation.
    It starts with a calculation of ranges, scaling factors and deviation,
    and translates the score based on the relationship between both scales.
    """
    if type(config) is not dict or type(old_score) is not int:
        return new_scale_max

    old_scale_min, old_scale_max = config["min"], config["max"]
    old_scale_min = 0 if old_scale_max == old_scale_min else old_scale_min

    # Adjust score in case it's lower or larger than the scale boundaries
    old_score = old_scale_max if old_score > old_scale_max else old_score
    old_score = old_scale_min if old_score < old_scale_min else old_score

    # Calculate the ranges of old and new scales
    new_scale_range = new_scale_max - new_scale_min
    old_scale_range = old_scale_max - old_scale_min

    # Calculate the scaling factor and deviation from the min
    scaling_factor = new_scale_range / old_scale_range
    old_scale_min_deviation = old_score - old_scale_min
    if old_scale_min_deviation < 0:
        return 0

    # Calculate the standardized score
    standardized_score = math.ceil(
        new_scale_min + (old_scale_min_deviation * scaling_factor)
    )
    return standardized_score
