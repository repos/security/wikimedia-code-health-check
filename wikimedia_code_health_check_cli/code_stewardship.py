#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import requests
import json
import os
import re


def get_task_backlog_config():
    phab_token = os.getenv("PHABRICATOR_TOKEN")
    if phab_token == "":
        raise ValueError(
            "Phabricator token environment variable not set - see README.md"
        )
    return {
        "phab_api_token": phab_token,  # global state from cli.py
        "phab_api_url": "https://phabricator.wikimedia.org/api/maniphest.search",
        "phab_project": "Code-Stewardship-Reviews",
        "phab_projects_file": "includes/phab_projects.txt",
        "scores": {"0": 10},
    }


def count_all(params):
    c = get_task_backlog_config()
    return_val = 0

    res = requests.post(c["phab_api_url"], params, timeout=5)
    if res.status_code == 200:
        response = json.loads(res.text)
        total = len(response["result"]["data"])
        after = response["result"]["cursor"]["after"]
    if after is None:
        return_val = total
    else:
        params["after"] = after
        return_val = total + count_all(params)

    return return_val


def get_open_code_stewardship_review(project_name):
    c = get_task_backlog_config()
    return_val = 0

    params = {
        "api.token": c["phab_api_token"],
        "constraints[projects][0]": c["phab_project"],
        "constraints[statuses][0]": "open",
        "constraints[statuses][1]": "stalled",
        "constraints[statuses][2]": "in progress",
        "constraints[projects][1]": project_name,
    }
    return_val = count_all(params)

    return return_val


def get_code_stewardship_review_score(project_name):
    c = get_task_backlog_config()
    total_score = 0

    phab_prefixes = [
        "mediawiki-extensions-",
        "mediawiki-skins-",
        "mediawiki-services-",
        "",
    ]

    with open(c["phab_projects_file"]) as file:
        while line := file.readline():
            for p in phab_prefixes:
                re_pat = "^" + p + project_name
                if re.match(re_pat, line, re.I):
                    project_name = p + project_name
                    break

    review_exists = get_open_code_stewardship_review(project_name)

    for s, v in c["scores"].items():
        if review_exists > int(s):
            total_score = int(v)
            break

    return total_score
