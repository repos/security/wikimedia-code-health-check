#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import glob
import json
import os
import re
import requests
import subprocess
from lxml import html
from pathlib import Path
import wikimedia_code_health_check_cli.util as util


def get_test_coverage_config():
    return {
        "scores": {
            "no_test_dirs": 10,
            "no_test_file_lines": {"5": 10, "50": 5, "150": 0},
            "poor_php_test_coverage": {"25": 10, "50": 5, "75": 2},
            "poor_js_test_coverage": {"25": 10, "50": 5, "75": 2},
        },
        "min": 4,
        "max": 30,
        "dependencies": ["npm"],
    }


def check_test_cov_dependencies(config):
    checked = {}
    for dep in config["dependencies"]:
        if dep not in checked:
            if util.check_if_dependency_exists(dep):
                checked[dep] = True
            else:
                checked[dep] = False
    return checked


def find_test_dirs(git_repo_path, test_dirs=["test", "tests"]):
    std_test_dir = ""
    base_dir = os.getcwd()
    os.chdir(git_repo_path)
    for d in test_dirs:
        if os.path.exists(d):
            std_test_dir = d
    os.chdir(base_dir)
    return std_test_dir


def get_test_coverage_lines(git_repo_path):
    total_test_lines = 0
    test_dir = find_test_dirs(git_repo_path)
    base_dir = os.getcwd()

    if test_dir != "":
        os.chdir(git_repo_path + "/" + test_dir)
        for fn in glob.glob("**/*.*", recursive=True):
            if os.path.isfile(fn):
                with open(fn, "rb") as f:
                    total_test_lines += sum(1 for _ in f)
    os.chdir(base_dir)

    return total_test_lines


def check_php_code_coverage(git_repo_path):
    """TODO: would be ideal to get this to run locally
    via phpunit, pcov, xdebug, etc.
    """
    php_cov_percent = 0
    base_cov_data_url = "https://doc.wikimedia.org/"
    base_cov_prefixes = ["cover-extensions", "cover-skins"]  # only support for now
    p = Path(git_repo_path)
    project_name = p.stem

    for pref in base_cov_prefixes:
        cov_url = base_cov_data_url + pref + "/" + project_name
        http_response = requests.get(cov_url, timeout=5)
        if http_response.status_code == 200:
            doc = html.fromstring(http_response.text)
            total_check = doc.xpath(
                '//table[@class="table table-bordered"]//tr[1]//td[1]//text()'
            )
            if "Total" in total_check:
                total_cov = doc.xpath(
                    '//table[@class="table table-bordered"]//tr[1]//td[2]//text()'
                )
                total_cov = " ".join(total_cov)
                total_cov = re.findall(r"\d+\.\d+", total_cov)
                total_cov = "".join(total_cov)
                php_cov_percent = total_cov
        else:
            continue
    return php_cov_percent


def check_js_code_coverage(git_repo_path):
    """see if coverage command exists in a root package.json"""
    js_cov_percent = 0
    npm_cov_results = ""
    base_dir = os.getcwd()

    os.chdir(git_repo_path)
    f = open("./package.json", "rb")
    pkg_json = json.load(f)

    for s in pkg_json["scripts"]:
        if s == "coverage":
            try:
                subprocess.run(
                    ["npm", "i", "--save-dev"],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.DEVNULL,
                )
            except subprocess.CalledProcessError as err:
                print(str(err))

            npm_cov_out = None
            try:
                npm_cov_out = subprocess.run(
                    ["npm", "run", "coverage"],
                    stdout=subprocess.PIPE,
                    stderr=subprocess.DEVNULL,
                )
            except subprocess.CalledProcessError as err:
                print(str(err))
            finally:
                npm_cov_results = str(npm_cov_out.stdout)

    cov_floats = [0]
    for line in npm_cov_results.split("\\n"):
        if "All files" in line:
            cov_floats = re.findall(r"\d+\.\d+", line)
    js_cov_percent = cov_floats[len(cov_floats) - 1]

    os.chdir(base_dir)

    return js_cov_percent


def calculate_test_coverage_score(git_repo_path="."):
    c = get_test_coverage_config()
    total_score = 0

    """check deps"""
    checked_deps = check_test_cov_dependencies(c)
    checked_deps_valid = False if False in checked_deps.values() else True

    if checked_deps_valid:
        if find_test_dirs(git_repo_path) == "":
            total_score += int(c["scores"]["no_test_dirs"])

        lines = get_test_coverage_lines(git_repo_path)
        for s, v in c["scores"]["no_test_file_lines"].items():
            if lines < int(s):
                total_score += int(v)
                break

        php_cov = check_php_code_coverage(git_repo_path)
        for s, v in c["scores"]["poor_php_test_coverage"].items():
            if php_cov != "" and float(php_cov) < int(s):
                total_score += int(v)
                break

        js_cov = check_js_code_coverage(git_repo_path)
        for s, v in c["scores"]["poor_js_test_coverage"].items():
            if js_cov != "" and float(js_cov) < int(s):
                total_score += int(v)
                break
    else:
        print("Unable to run lockfile scans due to missing dependencies:")
        for dep in checked_deps:
            if checked_deps[dep] is False:
                print(f"\n* {dep}\n")

    total_score = util.standardize_score(c, total_score)
    return total_score
