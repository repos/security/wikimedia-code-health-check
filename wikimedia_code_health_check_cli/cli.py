#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import click
import os
import wikimedia_code_health_check_cli.code_stewardship as code_stewardship
import wikimedia_code_health_check_cli.contributor_concentration as contributor_concentration  # noqa E501
import wikimedia_code_health_check_cli.data_management as data_management
import wikimedia_code_health_check_cli.git_utils as git_utils
import wikimedia_code_health_check_cli.non_auto_repo_commits as non_auto_repo_commits
import wikimedia_code_health_check_cli.language_guidelines as language_guidelines
import wikimedia_code_health_check_cli.package_management as package_management
import wikimedia_code_health_check_cli.search_projects as search_projects
import wikimedia_code_health_check_cli.staff_support as staff_support
import wikimedia_code_health_check_cli.static_analysis as static_analysis
import wikimedia_code_health_check_cli.task_backlog as task_backlog
import wikimedia_code_health_check_cli.test_coverage as test_coverage
import wikimedia_code_health_check_cli.unique_contributors as unique_contributors
import wikimedia_code_health_check_cli.vulnerable_packages as vulnerable_packages
from dotenv import load_dotenv


@click.command()
@click.argument("search", required=True, default="Graph")
@click.option(
    "-c",
    "--csv",
    is_flag=True,
    default=False,
    help="Display report output as a CSV.",
)
@click.option(
    "-fg",
    "--force-gerrit",
    is_flag=True,
    default=False,
    help="Only use Wikimedia Gerrit to search for this project's repository",
)
@click.option(
    "-fgh",
    "--force-github",
    is_flag=True,
    default=False,
    help="Only use Wikimedia Github to search for this project's repository",
)
@click.option(
    "-fgl",
    "--force-gitlab",
    is_flag=True,
    default=False,
    help="Only use Wikimedia Gitlab to search for this project's repository",
)
@click.option(
    "-vd",
    "--vuln-dep",
    is_flag=True,
    default=False,
    help="Run only the vulnerable dependency health check.",
)
@click.option(
    "-pm",
    "--pkg-mgmt",
    is_flag=True,
    default=False,
    help="Run only the package management checks.",
)
@click.option(
    "-tc",
    "--test-cov",
    is_flag=True,
    default=False,
    help="Run only the test coverage checks.",
)
@click.option(
    "-sa",
    "--sast",
    is_flag=True,
    default=False,
    help="Run only the sast health check.",
)
@click.option(
    "-nac",
    "--non-auto-commits",
    is_flag=True,
    default=False,
    help="Run only the non-auto-commits health check.",
)
@click.option(
    "-uc",
    "--uniq-contribs",
    is_flag=True,
    default=False,
    help="Run only the unique contributors health check.",
)
@click.option(
    "-cc",
    "--contrib-conc",
    is_flag=True,
    default=False,
    help="Run only the contributor concentration health check.",
)
@click.option(
    "-lg",
    "--lang-guidelines",
    is_flag=True,
    default=False,
    help="Run only the language guidelines health check.",
)
@click.option(
    "-sp",
    "--staff-supp",
    is_flag=True,
    default=False,
    help="Run only the staff support health check.",
)
@click.option(
    "-b",
    "--backlog",
    is_flag=True,
    default=False,
    help="Run only the Phabricator task backlog health check.",
)
@click.option(
    "-ocsr",
    "--open-csr",
    is_flag=True,
    default=False,
    help="Run only the Phabricator open code stewardship health check.",
)
def cli(
    search,
    csv,
    force_gerrit,
    force_github,
    force_gitlab,
    vuln_dep,
    pkg_mgmt,
    test_cov,
    sast,
    non_auto_commits,
    uniq_contribs,
    contrib_conc,
    lang_guidelines,
    staff_supp,
    backlog,
    open_csr,
):
    """env conf"""
    load_dotenv()
    repo_base_path = os.environ.get("GIT_CLONE_PATH", "./")

    """run all checks"""
    run_all_checks = not (
        vuln_dep
        or pkg_mgmt
        or test_cov
        or sast
        or non_auto_commits
        or uniq_contribs
        or contrib_conc
        or lang_guidelines
        or staff_supp
        or backlog
        or open_csr
    )

    proj_exlusions = [
        "ConfirmEdit/FancyCaptcha",
    ]

    """check project exlusion list"""
    if search in proj_exlusions:
        raise RuntimeError("%s has been excluded as an edge case" % search)

    """process @branch/tag syntax, if exists"""
    search, project_git_branch = git_utils.process_project_with_branch_or_tag(
        search
    )  # noqa E501

    """search for a git project/repo"""
    project_repo_url = search_projects.search_for_project_repo(
        search, force_gerrit, force_github, force_gitlab
    )

    """clone git repo, if found"""
    if project_repo_url != "":
        git_utils.attempt_local_clone_and_checkout(
            search, project_repo_url, project_git_branch
        )
    else:
        raise RuntimeError("No project repo URL was found.")

    """run various health checks and store result data"""
    wchc_results = {}
    if vuln_dep or run_all_checks:
        wchc_results[
            "vulnerable_packages"
        ] = vulnerable_packages.attempt_lockfile_scans(repo_base_path + search)
    if pkg_mgmt or run_all_checks:
        wchc_results[
            "package_management"
        ] = package_management.calculate_package_management_score(
            repo_base_path + search
        )
    if test_cov or run_all_checks:
        wchc_results["test_coverage"] = test_coverage.calculate_test_coverage_score(
            repo_base_path + search
        )
    if sast or run_all_checks:
        wchc_results["sast"] = static_analysis.attempt_sast_scan(
            repo_base_path + search
        )
    if non_auto_commits or run_all_checks:
        wchc_results[
            "non_auto_repo_commits"
        ] = non_auto_repo_commits.get_non_auto_commits(
            repo_base_path + search, project_git_branch
        )
    if uniq_contribs or run_all_checks:
        wchc_results["unique_contributors"] = unique_contributors.get_unique_contribs(
            repo_base_path + search, project_git_branch
        )
    if contrib_conc or run_all_checks:
        wchc_results[
            "contributor_concentration"
        ] = contributor_concentration.get_contrib_conc(
            repo_base_path + search, project_git_branch
        )
    if lang_guidelines or run_all_checks:
        wchc_results[
            "language_guidelines"
        ] = language_guidelines.calculate_lang_guidelines_score(repo_base_path + search)
    if staff_supp or run_all_checks:
        wchc_results["staff_support"] = staff_support.get_staff_support_score(
            repo_base_path + search, project_git_branch
        )
    if backlog or run_all_checks:
        wchc_results["task_backlog"] = task_backlog.get_task_backlog_score(search)
    if open_csr or run_all_checks:
        wchc_results[
            "code_stewardship"
        ] = code_stewardship.get_code_stewardship_review_score(search)

    """output report data"""
    data_management.output_report_results(
        search, project_git_branch, project_repo_url, wchc_results, csv
    )


if __name__ == "__main__":
    cli()
