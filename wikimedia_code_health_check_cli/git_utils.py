#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import git
import os
import shutil
import validators


def process_project_with_branch_or_tag(search):
    result = search.split("@")
    return_val = search, "master"
    if len(result) == 2 and result[1] != "":
        return_val = result[0], result[1]
    return return_val


def attempt_local_clone_and_checkout(
    project_name, project_repo_url, project_git_branch
):
    """attempt to clone local copy of repo"""
    return_val = True
    repo_base_path = os.getenv("GIT_CLONE_PATH")  # loaded from cli.py
    project_local_repo_path = repo_base_path + project_name
    valid_url = validators.url(project_repo_url)

    if valid_url is True:
        try:
            shutil.rmtree(project_local_repo_path, ignore_errors=True)
            g = git.Repo.clone_from(
                project_repo_url, project_local_repo_path
            )  # assumes public repo
            return_val = g.git.checkout(project_git_branch)
        except git.exc.GitError as err:
            return_val = str(err)
    else:
        raise RuntimeError(
            f"Invalid git project url was provided: {project_repo_url}."
        )  # noqa E501

    return return_val
