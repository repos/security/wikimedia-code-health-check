#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import json
import jsonschema
import re
import os

from wikimedia_code_health_check_cli import util


def get_pkg_mgmt_config():
    return {
        "php": {
            "pkgfile": "composer.json",
            "lockfile": "composer.lock",
            "pkgfile_schema": "includes/composer.schema.json",
        },
        "node": {
            "pkgfile": "package.json",
            "lockfile": "package-lock.json",
            "pkgfile_schema": "includes/package.schema.json",
        },
    }


def get_pkg_mgmt_scores_config():
    return {
        "no_std_pkgfiles": 10,
        "invalid_pkgfile_schema": 10,
        "non_semver_deps": 10,
        "min": 0,
        "max": 30,
    }


def check_pkgfiles_exists(git_repo_path, config):
    std_pkgfiles_exist = False
    std_pkgfiles = []

    # TODO: support nested lockfiles
    for lang, info in config.items():
        for ftype, file in info.items():
            # skip schema files (checked in check_for_valid_pkg_file_schema)
            # and composer.lock
            if ftype == "pkgfile_schema" or file == "composer.lock":
                continue
            file_path = git_repo_path + "/" + file
            if os.path.exists(file_path):
                std_pkgfiles.append(True)
            else:
                std_pkgfiles.append(False)

    if False not in std_pkgfiles:
        std_pkgfiles_exist = True
    return std_pkgfiles_exist


def check_for_valid_pkg_file_schema(git_repo_path, config):
    failed_validations = []
    valid_pkgfile_schemas = False

    for lang, info in config.items():
        pkgfile_path = git_repo_path + "/" + info["pkgfile"]
        pkgfile_schema_path = "./" + info["pkgfile_schema"]
        if os.path.exists(pkgfile_path) and os.path.exists(pkgfile_schema_path):
            pkgfile_data = open(pkgfile_path, "r").read()
            pkgfile_schema_data = open(pkgfile_schema_path, "r").read()
            pkgfile_data = json.loads(pkgfile_data)
            pkgfile_schema_data = json.loads(pkgfile_schema_data)

            try:
                jsonschema.validate(pkgfile_data, pkgfile_schema_data)
            except jsonschema.exceptions.ValidationError:
                failed_validations.append(pkgfile_path)

    if len(failed_validations) == 0:
        valid_pkgfile_schemas = True

    return valid_pkgfile_schemas


def check_for_non_semver_deps(git_repo_path, config):
    valid_dep_versions = True

    """TODO: might be nice to eventually verify integrity here"""
    for lang, info in config.items():
        pkgfile_data = {}
        pkgfile_path = git_repo_path + "/" + info["pkgfile"]
        if os.path.exists(pkgfile_path):
            pkgfile_data = open(pkgfile_path, "r").read()
            pkgfile_data = json.loads(pkgfile_data)
        for d, el in pkgfile_data.items():
            if re.match(r".{0,16}(dependencies|require).{0,16}", d, re.IGNORECASE):
                for dep, ver in el.items():
                    if re.match(
                        r"^(http|git|git\+ssh|git\+http|git\+https|git\+file)",
                        ver,
                        re.IGNORECASE,
                    ):
                        valid_dep_versions = (
                            False  # should be semver, numerical or similar
                        )

    return valid_dep_versions


def calculate_package_management_score(git_repo_path="."):
    c = get_pkg_mgmt_config()
    s = get_pkg_mgmt_scores_config()
    total_score = 0

    if not check_pkgfiles_exists(git_repo_path, c):
        total_score += int(s["no_std_pkgfiles"])

    if not check_for_valid_pkg_file_schema(git_repo_path, c):
        total_score += int(s["invalid_pkgfile_schema"])

    if not check_for_non_semver_deps(git_repo_path, c):
        total_score += int(s["non_semver_deps"])

    total_score = util.standardize_score(s, total_score)
    return total_score
