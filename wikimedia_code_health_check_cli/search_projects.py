#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import git
import json
import re
import requests
import validators
from urllib.parse import quote_plus
from shlex import quote as shlex_quote


def search_for_project_repo(search, force_gerrit, force_github, force_gitlab):
    """search for project repo url in a few different systems"""
    project_repo_url = ""
    no_force_options = not (force_gerrit or force_github or force_gitlab)
    valid_url = validators.url(search)

    if valid_url is True:
        project_repo_url = search

    if force_gerrit and project_repo_url == "":
        project_repo_url = maybe_find_at_gerrit(search)
    elif force_github and project_repo_url == "":
        project_repo_url = maybe_find_at_github(search)
    elif force_gitlab and project_repo_url == "":
        project_repo_url = maybe_find_at_gitlab(search)

    if project_repo_url == "" and no_force_options:
        project_repo_url = (
            maybe_find_at_gerrit(search)
            or maybe_find_at_github(search)
            or maybe_find_at_gitlab(search)
        )

    return project_repo_url


def maybe_find_at_gerrit(search):
    """try extensions, skins, services, no-prefix
    TODO: eventually support more project prefix types
    """
    gerrit_prefixes = [
        "mediawiki/extensions/",
        "mediawiki/skins/",
        "mediawiki/services/",
        "",
    ]

    active_project_found = False
    project_repo_url = ""
    gerrit_api_base_url = "https://gerrit.wikimedia.org/r/projects/"
    gerrit_base_repo_url = "https://gerrit.wikimedia.org/r/"

    count = 0
    lower_case_round = False
    while active_project_found is False:
        gerrit_project_name = gerrit_prefixes[count] + search
        gerrit_api_url = (
            gerrit_api_base_url
            + "?query=name:"
            + quote_plus(gerrit_project_name)  # noqa E501
        )
        http_response = requests.get(gerrit_api_url, timeout=5)
        if http_response.status_code == 200:
            response_text_processed = "".join(
                http_response.text.split("\n")[1:]
            )  # silly gerrit
            json_from_gerrit_api = json.loads(response_text_processed)
            if (
                len(json_from_gerrit_api) == 1
                and json_from_gerrit_api[0]["state"] == "ACTIVE"
            ):
                active_project_found = True
                project_repo_url = gerrit_base_repo_url + gerrit_project_name
        else:
            raise RuntimeError(
                "Bad http respone from: " + shlex_quote(gerrit_api_url)
            )  # noqa E501

        count += 1
        if count > len(gerrit_prefixes) - 1 and lower_case_round is False:
            search = search[0].lower() + search[1:]
            count = 0
            lower_case_round = True
        elif count > len(gerrit_prefixes) - 1 and lower_case_round is True:
            break

    return project_repo_url


def maybe_find_at_github(search):
    """try extensions, skins, services, no-prefix
    TODO: eventually support more project prefix types
    """
    github_prefixes = [
        "mediawiki-extensions-",
        "mediawiki-skins-",
        "mediawiki-services-",
        "",
    ]

    project_repo_url = ""
    active_project_found = False
    github_base_repo_url = "https://github.com/wikimedia/"

    count = 0
    while active_project_found is False:
        github_project_repo_url = (
            github_base_repo_url + github_prefixes[count] + search + ".git"
        )

        """just check refs via ls-remote"""
        try:
            remote_refs = {}
            g = git.cmd.Git()
            remote_refs = g.ls_remote(github_project_repo_url)
            re1 = re.compile(
                "refs/heads/(master|main)\n"
            )  # just find a master/main branch
            if re1.search(remote_refs):
                active_project_found = True
                project_repo_url = github_project_repo_url
        except git.exc.GitCommandError as err:
            err_msg = "".join(str(err).split("\n"))
            if (
                "stderr: 'fatal: no path specified;" not in err_msg
                and "stderr: 'remote: Repository not found" not in err_msg
            ):
                raise RuntimeError("Bad git command: " + err_msg)

        count += 1
        if count > len(github_prefixes) - 1:
            break

    return project_repo_url


def maybe_find_at_gitlab(search):
    """try to find project via gitlab's (public) general api search, no assumed prefixes/namespaces"""  # noqa E501
    active_project_found = False
    project_repo_url = ""
    gitlab_api_base_url = (
        "https://gitlab.wikimedia.org/api/v4/projects?search="  # noqa E501
    )

    while active_project_found is False:
        gitlab_api_url = gitlab_api_base_url + quote_plus(search)
        http_response = requests.get(gitlab_api_url, timeout=5)
        if http_response.status_code == 200:
            json_from_gitlab_api = json.loads(http_response.text)
            if len(json_from_gitlab_api) > 1:
                raise RuntimeError(
                    "Mutliple Gitlab projects found for '"
                    + search
                    + "' - try being more specific with your project search name."  # noqa E501
                )
            elif len(json_from_gitlab_api) == 1:
                active_project_found = True
                project_repo_url = (
                    json_from_gitlab_api[0]["http_url_to_repo"] or ""
                )  # noqa E501
            else:
                break
        else:
            raise RuntimeError(
                "Bad http respone from: " + shlex_quote(gitlab_api_url)
            )  # noqa E501

    return project_repo_url
