#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from unittest.mock import patch, mock_open
from wikimedia_code_health_check_cli.language_guidelines import (
    get_lang_guidelines_config,
    get_lang_guidelines_scores_config,
    check_pkgfiles_exist,
    check_for_valid_dep_keys,
    calculate_lang_guidelines_score,
)


def test_get_lang_guidelines_config():
    """test for valid config dict"""
    c = get_lang_guidelines_config()
    for k1, v1 in c.items():
        assert isinstance(k1, str)
        assert isinstance(v1, dict)
        for k2, v2 in v1.items():
            assert isinstance(k2, str)
            if isinstance(v2, dict):
                for k3, v3 in v2.items():
                    assert isinstance(k3, str)
                    assert isinstance(v3, list)
            else:
                assert isinstance(v2, str)


def test_get_lang_guidelines_scores_config():
    """test for valid scores config dict"""
    s = get_lang_guidelines_scores_config()
    for k1, v1 in s.items():
        assert isinstance(k1, str)
        assert isinstance(v1, dict)
        for k2, v2 in v1.items():
            assert isinstance(int(k2), int)
            assert isinstance(v2, int)


@patch("os.path.exists")
def test_check_pkgfiles_exist(mock_exists_function):
    """test for valid package files within repos dir"""
    mock_exists_function.return_value = True
    git_path = "/a/fake/path/for/testing/"
    c = get_lang_guidelines_config()
    assert check_pkgfiles_exist(git_path, c) is True

    mock_exists_function.return_value = False
    assert check_pkgfiles_exist(git_path, c) is False


@patch("os.path.exists")
@patch("builtins.open", mock_open(read_data='{"test": "test"}'))
def test_check_for_valid_dep_keys_bad_values(mock_exists_function):
    """test for checking the number of valid dependency keys (bad data)"""
    mock_exists_function.return_value = True
    git_path = "/a/fake/path/for/testing/"
    c = get_lang_guidelines_config()
    assert check_for_valid_dep_keys(git_path, c) == 0


@patch("os.path.exists")
@patch(
    "builtins.open",
    mock_open(
        read_data='{"require-dev": {"mediawiki/mediawiki-codesniffer": "1.2.3"}}'
    ),
)
def test_check_for_valid_dep_keys_good_values(mock_exists_function):
    """test for checking the number of valid dependency keys (good data)"""
    mock_exists_function.return_value = True
    git_path = "/a/fake/path/for/testing/"
    c = get_lang_guidelines_config()
    assert check_for_valid_dep_keys(git_path, c) == 1


@patch("wikimedia_code_health_check_cli.language_guidelines.check_pkgfiles_exist")
@patch("wikimedia_code_health_check_cli.language_guidelines.check_for_valid_dep_keys")
def test_calculate_lang_guidelines_score(cfvdk_func, cpe_func):
    """test for checking the final language guidelines risk score"""
    s = get_lang_guidelines_scores_config()
    cfvdk_func.return_value = 10
    cpe_func.return_value = True
    assert calculate_lang_guidelines_score() == s["valid_dep_keys"]["10"]
