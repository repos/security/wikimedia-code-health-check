#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from pathlib import PosixPath
from unittest.mock import patch
from wikimedia_code_health_check_cli.vulnerable_packages import (
    get_lockfile_config,
    get_vuln_pkgs_scores_config,
    get_other_dependencies_config,
    check_lockfile_dependencies,
    get_package_file_paths,
    maybe_find_other_vulnerable_dependencies,
)


def test_get_lockfile_config():
    """test for valid config dict"""
    c = get_lockfile_config()
    for k1, v1 in c.items():
        assert isinstance(k1, str)
        assert isinstance(v1, dict)
        for k2, v2 in v1.items():
            assert isinstance(k2, str)
            assert isinstance(v2, str)


def test_get_vuln_pkgs_scores_config():
    """test for valid scores config dict"""
    s = get_vuln_pkgs_scores_config()
    for k, v in s.items():
        assert isinstance(k, str)
        assert isinstance(v, int)


def test_get_other_dependencies_config():
    """test for valid other deps config"""
    o = get_other_dependencies_config()
    for k, v in o.items():
        assert isinstance(k, str)
        assert isinstance(v, list)


@patch("wikimedia_code_health_check_cli.util.check_if_dependency_exists")
def test_check_lockfile_dependencies(mock_dep_exists_func):
    c = get_lockfile_config()

    # all 5 current deps for a key in lockfile_config dict are found
    mock_returns = [True, True, True, True, True]
    mock_dep_exists_func.side_effect = mock_returns
    vals = check_lockfile_dependencies(c)
    assert isinstance(vals, dict)
    for i, (k, v) in enumerate(vals.items()):
        assert v == mock_returns[i]

    # only 3 deps for a key in lockfile_config dict are found
    mock_returns = [True, True, False, True, False]
    mock_dep_exists_func.side_effect = mock_returns
    vals = check_lockfile_dependencies(c)
    assert isinstance(vals, dict)
    for i, (k, v) in enumerate(vals.items()):
        assert v == mock_returns[i]

    # no deps for a key in lockfile_config dict are found
    mock_dep_exists_func.side_effect = [False, False, False, False, False]
    vals = check_lockfile_dependencies(c)
    assert isinstance(vals, dict)
    for k, v in vals.items():
        assert v is False


@patch("pathlib.Path.rglob")
def test_get_package_file_paths(mock_rglob):
    c = get_lockfile_config()
    mock_rglob_return = [
        [PosixPath("local_repos/something/package.json")],
        [PosixPath("local_repos/something/composer.json")],
    ]
    mock_rglob.side_effect = mock_rglob_return
    paths = get_package_file_paths(c)
    assert isinstance(paths, dict)
    for i, (p, l) in enumerate(paths.items()):
        assert p == mock_rglob_return[i][0].as_posix()
        assert isinstance(l, str)


@patch("pathlib.Path.rglob")
@patch("os.path.isfile")
def test_maybe_find_other_vulnerable_dependencies(mock_isfile, mock_rglob):
    o = get_other_dependencies_config()
    git_repo_path = "local_repos"

    # one bad file found, should result in return of 1
    mock_rglob_return = [[PosixPath("local_repos/lib/bad.js")]]
    mock_rglob.side_effect = mock_rglob_return
    mock_isfile.return_value = True
    assert maybe_find_other_vulnerable_dependencies(o, git_repo_path) == 1

    # no bad files found, should result in return of 0
    mock_rglob_returns = [[PosixPath("a/bad/path")]]
    mock_rglob.side_effect = mock_rglob_returns
    mock_isfile.return_value = False
    assert maybe_find_other_vulnerable_dependencies(o, git_repo_path) == 0


def test_attempt_lockfile_scans():
    # TODO: implement
    pass
