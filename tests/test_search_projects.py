#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import git
import pytest
import responses
from unittest.mock import patch
from wikimedia_code_health_check_cli.search_projects import (
    search_for_project_repo,
    maybe_find_at_gerrit,
    maybe_find_at_github,
    maybe_find_at_gitlab,
)


def test_search_for_project_repo_url():
    """test a valid url - should just be returned after passing
    validators.url
    """
    url = "https://example.com/a/git/repo.git"
    assert search_for_project_repo(url, False, False, False) == url


@responses.activate
def test_search_for_gerrit_invalid_project():
    """test search"""
    test_search = "A_bad_project_that_does_not_exist"
    test_ext_prefix = "mediawiki/extensions/"

    """add mocked response for 404"""
    responses.add(
        responses.GET,
        "https://gerrit.wikimedia.org/r/projects/?query=name:"
        + test_ext_prefix
        + test_search,
        json={"error": "not found"},
        status=404,
    )

    with pytest.raises(RuntimeError):
        maybe_find_at_gerrit(test_search)


@responses.activate
def test_search_for_gerrit_project_valid_repos():
    test_searches = [
        "Examples",  # extension
        "MinervaNeue",  # skin
        "citoid",  # service
        "analytics/aggregator",  # random stand-alone
    ]

    test_gerrit_prefixes = [
        "mediawiki/extensions/",
        "mediawiki/skins/",
        "mediawiki/services/",
        "",
    ]
    test_gerrit_repo_url = "https://gerrit.wikimedia.org/r/"

    """add mocked response for repos"""
    for s in test_searches:
        for p in test_gerrit_prefixes:
            repo_state_upper = "INACTIVE"
            repo_state_lower = "INACTIVE"
            if s == "MinervaNeue" and p == "mediawiki/skins/":
                repo_state_upper = "ACTIVE"
                repo_state_lower = "INACTIVE"
            elif (
                (s == "Examples" and p == "mediawiki/extensions/")
                or (s == "citoid" and p == "mediawiki/services/")
                or (s == "analytics/aggregator" and p == "")
            ):
                repo_state_upper = "ACTIVE"
                repo_state_lower = "ACTIVE"
            responses.add(
                responses.GET,
                "https://gerrit.wikimedia.org/r/projects/?query=name:" + p + s,
                body='gerrit junk\n[{"name": "'
                + p
                + s
                + '", "state": "'
                + repo_state_upper
                + '"}]\n',
                content_type="application/json",
                status=200,
            )
            responses.add(
                responses.GET,
                "https://gerrit.wikimedia.org/r/projects/?query=name:"
                + p
                + s[:1].lower()
                + s[1:],
                body='gerrit junk\n[{"name": "'
                + p
                + s[:1].lower()
                + s[1:]
                + '", "state": "'
                + repo_state_lower
                + '"}]\n',
                content_type="application/json",
                status=200,
            )

    for s in test_searches:
        for p in test_gerrit_prefixes:
            if (
                (s == "Examples" and p == "mediawiki/extensions/")
                or (s == "citoid" and p == "mediawiki/services/")
                or (s == "analytics/aggregator" and p == "")
            ):
                assert (
                    maybe_find_at_gerrit(s[:1].lower() + s[1:])
                    == test_gerrit_repo_url + p + s[:1].lower() + s[1:]
                )
            elif s == "MinervaNeue" and p == "mediawiki/skins/":
                assert maybe_find_at_gerrit(s) == test_gerrit_repo_url + p + s


@patch("git.cmd.Git")
def test_search_for_github_invalid_repo(mock_git_cmd):
    """mock git ls_remote command"""
    mock_git_cmd.return_value.ls_remote.return_value = "A bad value"
    test_search = "A_bad_project_that_does_not_exist"

    assert maybe_find_at_github(test_search) == ""


@patch("git.cmd.Git")
def test_search_for_github_bad_git_cmd(mock_git_cmd):
    """mock git ls_remote command"""
    mock_git_cmd.return_value.ls_remote.side_effect = git.exc.GitCommandError(
        "Something bad..."
    )
    test_search = "A_bad_project_that_does_not_exist"

    with pytest.raises(RuntimeError):
        maybe_find_at_github(test_search)


@patch("git.cmd.Git")
def test_search_for_github_valid_repos(mock_git_cmd):
    mock_git_cmd.return_value.ls_remote.side_effect = [
        "A bad value",
        "A bad value",
        "A bad value",
        "02340293834029384480 refs/heads/main\n",
        "A bad value",
        "A bad value",
        "A bad value",
        "34209348209384029384 refs/heads/main\n",
    ]
    test_searches = [
        "restbase",
        "service-template-node",
    ]
    git_repo_suffix = ".git"
    github_base_repo_url = "https://github.com/wikimedia/"

    for s in test_searches:
        assert (
            maybe_find_at_github(s)
            == github_base_repo_url + s + git_repo_suffix  # noqa E501
        )


@responses.activate
def test_search_for_gitlab_invalid_repo():
    test_search = "A_bad_project_that_does_not_exist"

    """add mocked response 404"""
    responses.add(
        responses.GET,
        "https://gitlab.wikimedia.org/api/v4/projects?search=" + test_search,
        json={"error": "not found"},
        status=404,
    )

    with pytest.raises(RuntimeError):
        maybe_find_at_gitlab(test_search)


@responses.activate
def test_search_for_gitlab_multiple_repos():
    test_search = "security"

    """add mocked response 200, but multiple projects"""
    responses.add(
        responses.GET,
        "https://gitlab.wikimedia.org/api/v4/projects?search=" + test_search,
        json=[
            {"id": 123, "description": "A bad project"},
            {"id": 456, "description": "Another bad project"},
            {"id": 789, "description": "Yet another bad project"},
        ],
        status=200,
    )

    with pytest.raises(RuntimeError):
        maybe_find_at_gitlab(test_search)


@responses.activate
def test_search_for_gitlab_valid_repos():
    test_search = "wikimedia-code-health-check"
    test_search_repo = (
        "https://gitlab.wikimedia.org/repos/security/" + test_search + ".git"
    )

    """add mocked response 200, but multiple projects"""
    responses.add(
        responses.GET,
        "https://gitlab.wikimedia.org/api/v4/projects?search=" + test_search,
        json=[
            {
                "id": 123,
                "description": "wikimedia-code-health-check",
                "http_url_to_repo": "https://gitlab.wikimedia.org/repos/security/wikimedia-code-health-check.git",  # noqa E501
            },
        ],
        status=200,
    )

    assert maybe_find_at_gitlab(test_search) == test_search_repo
