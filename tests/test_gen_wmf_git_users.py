#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import argparse
import os
import re
import responses
import sys
import validators
from pathvalidate import sanitize_filepath
from unittest.mock import patch


# hack for unpackaged script
root_path = os.curdir
if sys.path:
    for p in sys.path:
        if re.match(".+wikimedia-code-health-check$", p):
            root_path = p
sys.path.insert(0, "".join([root_path, "/scripts"]))
import gen_wmf_git_users  # noqa: E402


def test_get_config():
    c = gen_wmf_git_users.get_config()
    for k, v in enumerate(c):
        if k == "ldap_base_url":
            assert validators.url(v)
        if k == "wmf_ldap_url":
            assert validators.url(v)
        if k == "csv_file_path":
            assert v == sanitize_filepath(v)
        if k == "edge_cases_add":
            for i, (k, v) in enumerate(v):
                assert k is str and validators.email(v)
        if k == "edge_cases_remove":
            for i, (k, v) in enumerate(v):
                assert k is str and validators.email(v)


@patch(
    "argparse.ArgumentParser.parse_known_args",
    return_value=[
        argparse.Namespace(csv_file_path="wmf_staff.csv", debug=True, limit=10),
        ["wmf_staff.csv", "-d", "-l", "10"],
    ],
)
def test_parse_cli_args(mock_args):
    c = gen_wmf_git_users.get_config()
    args = gen_wmf_git_users.parse_cli_args(c)
    assert args.csv_file_path == "wmf_staff.csv"
    assert args.debug is True
    assert args.limit == 10


@patch(
    "argparse.ArgumentParser.parse_known_args",
    return_value=[
        argparse.Namespace(csv_file_path="wmf_staff.csv", debug=True, limit=10),
        ["wmf_staff.csv", "-d", "-l", "10"],
    ],
)
def test_update_config_from_cli_args(mock_args):
    c = gen_wmf_git_users.get_config()
    a = gen_wmf_git_users.parse_cli_args(c)
    uc = gen_wmf_git_users.update_config_from_cli_args(c, a)
    assert uc["csv_file_path"] == "wmf_staff.csv"
    assert uc["debug"] is True
    assert uc["limit"] == 10
    assert validators.url(uc["ldap_base_url"])
    assert validators.url(uc["wmf_ldap_url"])
    assert uc["csv_file_path"] == sanitize_filepath(uc["csv_file_path"])


@responses.activate()
def test_search_ldap_api():
    c = gen_wmf_git_users.get_config()
    responses.add(
        responses.GET,
        c["wmf_ldap_url"],
        body='<html><body><ul><li><a href="https://wikitech.wikimedia.org/wiki/User:Somebody">User:Somebody</a> (<a href="/user/somebody">more info</a>)</li></ul></body></html>',  # noqa: E501
        status=200,
    )
    responses.add(
        responses.GET,
        "".join([c["ldap_base_url"], "/user/somebody"]),
        body='<html><body><ul><li>Wikitech: <a href="https://wikitech.wikimedia.org/wiki/User:Somebody">User:Somebody</a></li><li>Shell username: <code>somebody</code></li><li>Email: <code>somebody at wikimedia dot org</code></li></ul></body></html>',  # noqa: E501
        status=200,
    )

    test_user_dict = {}
    test_user_dict.update(c["edge_cases_add"])
    for un in test_user_dict.copy():
        if (
            un in c["edge_cases_remove"].keys()
            or test_user_dict[un] in c["edge_cases_remove"].values()
        ):
            del test_user_dict[un]
    test_user_dict["Somebody"] = "somebody@wikimedia.org"

    assert gen_wmf_git_users.search_ldap_api(c) == test_user_dict


def test_output_to_file_or_terminal_print(capsys):
    d = {"Somebody": "somebody@wikimedia.org"}
    c = gen_wmf_git_users.get_config()

    # test debug = true, print
    c["debug"] = True
    gen_wmf_git_users.output_to_file_or_terminal(d, c)
    out, err = capsys.readouterr()
    assert out == "".join([str(d), "\n"])


def test_output_to_file_or_terminal_file(tmpdir):
    d = {"Somebody": "somebody@wikimedia.org"}
    c = gen_wmf_git_users.get_config()

    # test debug = false, file write
    tmp_file = tmpdir.join("test_output.csv")
    c["debug"] = False
    c["csv_file_path"] = tmp_file.strpath
    gen_wmf_git_users.output_to_file_or_terminal(d, c)
    assert tmp_file.read() == "Somebody,somebody@wikimedia.org\n"
