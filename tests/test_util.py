#!/usr/bin/env python3
# -*- coding: utf-8 -*- #


from wikimedia_code_health_check_cli.util import standardize_score


def test_standardize_score_with_regular_scales():
    score, config = 25, {"min": 10, "max": 30}

    # test with a 10-point scale
    output = standardize_score(config, score)
    assert output == 8

    # test with a 5-point scale
    output = standardize_score(config, score, 1, 5)
    assert output == 4

    # test with a 3-point scale
    output = standardize_score(config, score, 1, 3)
    assert output == 3


def test_standardize_score_with_unexpected_scales():
    # test with score which is greater than the scale
    output = standardize_score({"min": 10, "max": 20}, 50)
    assert output == 10

    # test with score which is smaller than the scale
    output = standardize_score({"min": 2, "max": 10}, 1)
    assert output == 0

    # test with score which is negative values
    output = standardize_score({"min": 2, "max": 10}, -6)
    assert output == 0

    # test with score which is string or other object
    output = standardize_score({"min": 2, "max": 10}, "")
    assert output == 10
