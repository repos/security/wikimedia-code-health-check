#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import os
import pytest
from unittest.mock import patch, mock_open
from wikimedia_code_health_check_cli.task_backlog import (
    get_task_backlog_config,
    backlog_size,
    higher_priorities,
    older_than_six_months,
    get_task_backlog_score,
)


@patch.dict(os.environ, {"PHABRICATOR_TOKEN": ""})
def test_get_task_backlog_config():
    with pytest.raises(ValueError):
        get_task_backlog_config()


def data_provider_for_count_tests(func):
    def wrapper(*args, **kwargs):
        """set api test data"""
        api_test_data_open = '{ "result": { "data": ['
        api_test_data_close = '], "cursor": { "after": null } } }'
        api_test_data_item = (
            '{ "id": 123456, "type": "TASK", "phid": "PHID-TASK-c3ab8ff13720e8ad9047" }'
        )

        """ create test data array  """
        kwargs["test_data"] = [
            [404, "", 0],
            [200, "[]", 0],
            [200, api_test_data_open + api_test_data_item + api_test_data_close, 1],
            [
                200,
                api_test_data_open
                + ", ".join([api_test_data_item] * 50)
                + api_test_data_close,
                50,
            ],
        ]

        """ create test data array for test_get_task_backlog_score """
        if func.__name__ == "test_get_task_backlog_score":
            kwargs["test_data"] = [
                [404, "", 0],
                [200, "[]", 0],
                [200, api_test_data_open + api_test_data_item + api_test_data_close, 0],
                [
                    200,
                    api_test_data_open
                    + ", ".join([api_test_data_item] * 50)
                    + api_test_data_close,
                    2,
                ],
                [
                    200,
                    api_test_data_open
                    + ", ".join([api_test_data_item] * 51)
                    + api_test_data_close,
                    4,
                ],
                [
                    200,
                    api_test_data_open
                    + ", ".join([api_test_data_item] * 101)
                    + api_test_data_close,
                    7,
                ],
                [
                    200,
                    api_test_data_open
                    + ", ".join([api_test_data_item] * 301)
                    + api_test_data_close,
                    10,
                ],
            ]

        func(*args, **kwargs)

    return wrapper


@data_provider_for_count_tests
@patch("requests.post")
def test_backlog_size(mock_post, test_data=[]):
    for count, test_item_list in enumerate(test_data):
        mock_post.return_value.status_code = test_item_list[0]
        mock_post.return_value.text = test_item_list[1]
        if count < len(test_data) - 1:
            result = backlog_size("testProject")
        else:
            result = backlog_size("testProject", True)
        assert result == test_item_list[2]


@data_provider_for_count_tests
@patch("requests.post")
def test_higher_priorities(mock_post, test_data=[]):
    for test_item_list in test_data:
        mock_post.return_value.status_code = test_item_list[0]
        mock_post.return_value.text = test_item_list[1]
        result = higher_priorities("testProject")
        assert result == test_item_list[2]


@data_provider_for_count_tests
@patch("requests.post")
def test_older_than_six_months(mock_post, test_data=[]):
    for test_item_list in test_data:
        mock_post.return_value.status_code = test_item_list[0]
        mock_post.return_value.text = test_item_list[1]
        result = older_than_six_months("testProject")
        assert result == test_item_list[2]


@data_provider_for_count_tests
@patch("requests.post")
@patch("builtins.open", mock_open(read_data="testProject"))
def test_get_task_backlog_score(mock_post, test_data=[]):
    for test_item_list in test_data:
        mock_post.return_value.status_code = test_item_list[0]
        mock_post.return_value.text = test_item_list[1]
        score = get_task_backlog_score("testProject")
        assert score == test_item_list[2]
