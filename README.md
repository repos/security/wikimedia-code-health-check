# Wikimedia Code Health Check

A simple cli tool that looks at a handful of metrics and attempts to produce a health score / risk rating for a given Wikimedia code repository.

This tool takes the repo name, finds the source code on gerrit and additionally looks at github.com/wikimedia and gitlab.wikimedia.org if it's not found. The found repo is locally cloned and several checks are run against the code, ranging from package management verification to open code stewardship reviews to analyzing the project's Phabricator task backlog. All of the checks are configurable and there is an option to output the data in tabular and CSV formats.

## Installing

1. ```git clone "https://gitlab.wikimedia.org/repos/security/wikimedia-code-health-check.git"```

## Usage

### .env file

Please take a look the [example env file](env.example) and rename to `.env` and update the following values:
* `PHABRICATOR_TOKEN` is a required environment variable
* `GIT_CLONE_PATH` is set to a default path of `./`
* Make sure that `GIT_CLONE_PATH` has a closing forward slash as shown in the example env file

### Dependencies

1. Python: see [pyproject.toml](pyproject.toml)
2. Package management: [composer](https://getcomposer.org/) and [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)
3. [SCA](https://en.wikipedia.org/wiki/Software_composition_analysis): [osv-scanner](https://google.github.io/osv-scanner/installation/)
4. [SAST](https://en.wikipedia.org/wiki/Static_application_security_testing): [semgrep](https://semgrep.dev/docs/getting-started/)
5. [Git](https://git-scm.com/)

### CLI Usage

```
# install dependencies
$ poetry install

# basic help
$ poetry run wchc --help

# health checks can be run individually or all at once (no option flags)
# all checks can take a few minutes to run and produce results
# it is important to note that these results are risk _score_ and not raw values
$ poetry run wchc Graph@REL1_39   # specify a branch

$ poetry run wchc Kartographer   # default to master or main branch

$ poetry run wchc --sast --force-github MobileFrontend   # only run sast tests and use github repo
```

## Script usage
```
# to generate the includes/wmf_staff.csv file, run:
$ poetry run python scripts/gen_wmf_git_users.py

# this will parse ldap.toolforge.org/groups/wmf and attempt to build a relevant list
# of current wmf staff members and their likely git usernames/emails

# to run the health check cli over a batch of repositories
# (notably Wikimedia-deployed extensions and skins), use:
$ poetry run python scripts/cli_runner.py

# by default, this ^ will use https://noc.wikimedia.org/conf/wmf-config/extension-list
# as its repository source list 
```

## TODO

1. Improve unit test/code coverage
2. Better sandbox/containerize certain dependencies (npm being an obvious one)
3. Implement a docker-compose
4. Likely opportunities to further modularize code

## Support

File a bug within [Phabricator](https://phabricator.wikimedia.org/maniphest/) and tag [#security-team](https://phabricator.wikimedia.org/project/profile/1179/).

## Contributing

Merge requests are always welcome.  For bugs and feature requests, please see support process noted above.  This project is governed by the [MediaWiki Code of Conduct](https://www.mediawiki.org/wiki/Code_of_Conduct).

## Authors

* **Scott Bassett** [sbassett@wikimedia.org]

## License

This project is licensed under the Apache 2.0 License - see the [LICENSE](LICENSE) file for details.
